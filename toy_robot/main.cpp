//
//  main.cpp
//  toy_robot
//
//  Created by Reinier Veral on 9/12/22.
//

#include <iostream>
#include "robot.hpp"

using namespace std;

int main(int argc, const char * argv[]) {
    Robot robot;
    cout << NORTH << EAST << SOUTH << WEST << endl;
    robot.placeRobot(2, 2, NORTH);
    robot.getStatus();
    robot.rotateRobot(LEFT);
    robot.getStatus();
    robot.rotateRobot(LEFT);
    robot.getStatus();
    robot.rotateRobot(LEFT);
    robot.getStatus();
    robot.rotateRobot(LEFT);
    robot.getStatus();
    robot.moveRobot();
    robot.getStatus();
    robot.moveRobot();
    robot.getStatus();
    robot.moveRobot();
    robot.getStatus();
    
    return 0;
}

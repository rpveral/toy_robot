//
//  robot.cpp
//  toy_robot
//
//  Created by Reinier Veral on 9/12/22.
//

#include <iostream>
#include "robot.hpp"

using namespace std;

void Robot::set_xpos(int x) {
    xpos = x;
}

void Robot::set_ypos(int y) {
    ypos = y;
}

int Robot::get_xpos() {
    return xpos;
}

int Robot::get_ypos() {
    return ypos;
}

void Robot::set_direction(enum direction dir){
    direction = dir;
}

enum direction Robot::get_direction() {
    return direction;
}

void Robot::placeRobot(int x, int y, enum direction dir) {
    set_xpos(x);
    set_ypos(y);
    set_direction(dir);
}

void Robot::moveRobot() {
    switch(direction) {
        case NORTH:
            if(xpos < MAX_XPOS - 1) {
                xpos++;
            }
            break;
        case EAST:
            if(ypos < MAX_YPOS - 1) {
                ypos++;
            }
            break;
        case SOUTH:
            if(xpos > 0) {
                xpos--;
            }
            break;
        case WEST:
            if(ypos > 0) {
                ypos--;
            }
            break;
    }
}

void Robot::rotateRobot(enum turn_direction turn_dir) {
    int stat_dir;
    if(turn_dir == LEFT) {
        stat_dir = static_cast<int>(direction);
        stat_dir--;
        if(stat_dir < static_cast<int>(NORTH)){
            set_direction(WEST);
        }
        else {
            set_direction(static_cast<enum direction>(stat_dir));
        }
    }
    else if(turn_dir == RIGHT) {
        stat_dir = static_cast<int>(direction);
        stat_dir++;
        if(stat_dir > static_cast<int>(WEST)){
            set_direction(NORTH);
        }
        else {
            set_direction(static_cast<enum direction>(stat_dir));
        }
    }
    else {
        
    }
}

void Robot::getStatus(){
    std::cout << get_xpos() << ", " << get_ypos() << ", " << const_direction[get_direction()]  << std::endl;
}


//
//  robot.hpp
//  toy_robot
//
//  Created by Reinier Veral on 9/12/22.
//

#ifndef robot_hpp
#define robot_hpp
#include <string>

#endif /* robot_hpp */

enum direction {NORTH, EAST, SOUTH, WEST};
enum turn_direction {LEFT, RIGHT};
constexpr std::string_view const_direction[] { "NORTH", "EAST", "SOUTH", "WEST"};
const int MAX_XPOS = 5;
const int MAX_YPOS = 5;

class Robot {
private:
    int xpos;
    int ypos;
    direction direction;
public:
    Robot() {

    }
        
    void set_xpos(int);
    void set_ypos(int);
    void set_direction(enum direction);
    int get_xpos();
    int get_ypos();
    enum direction get_direction();
    
    void placeRobot(int, int, enum direction) ;
    void moveRobot();
    void rotateRobot(enum turn_direction);
    void getStatus();
};

